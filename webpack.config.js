module.exports = {
    entry: './ui/index.js',
    output: {
        path: __dirname + '/ui',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' }
        ]
    }
}