# Movie searcher

Powered by https://www.themoviedb.org/

To run the app:
```sh
$ npm install
$ npm start
```

App will now be running on http://localhost:8080

To test the app:
```sh
$ npm install
$ npm test
```

npm test output a code coverage report to ./coverage

Todo:

- Add pagination to results
- Debouce API requests