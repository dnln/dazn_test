import React from 'react';
import { FormGroup, FormControl } from 'react-bootstrap';

class Search extends React.Component {

    constructor () {
        super();

        this.handleSearchTermUpdate = this.handleSearchTermUpdate.bind(this);
    }

    // get a list of popular movies as soon as the component mounts
    componentDidMount () {
        this.getPopularMovies();
    }

    handleSearchTermUpdate (e) {
        // e.target.value contains search box input value
        if (e.target.value) {
            const self = this;

            const searchTerm = encodeURI(e.target.value);

            const searchMovies = function (response) {
                // response contains pagination info, which could be implemented in future.
                // at the minute, only interested in results.
                const movies = JSON.parse(response).results;
                self.props.setMovies({ movies: movies, isPopularMovies: false });
                // clear old errors
                self.props.setError(null);
            };

            const searchMoviesError = function (err) {
                self.props.setError(err);
            };

            this.props.theMovieDB.search.getMovie({ query: searchTerm }, searchMovies, searchMoviesError);
        } else {
            // list all popular movies if no search term is entered
            this.getPopularMovies();
        }
    }

    getPopularMovies () {
        const self = this;

        const listPopularMovies = function (response) {
            // response contains pagination info, which could be implemented in future.
            // at the minute, only interested in results.
            const movies = JSON.parse(response).results;
            self.props.setMovies({ movies: movies, isPopularMovies: true });
            // clear old errors
            self.props.setError(null);
        };

        const listPopularMoviesError = function (err) {
            self.props.setError(err);
        };

        this.props.theMovieDB.movies.getPopular({}, listPopularMovies, listPopularMoviesError);
    }

    render () {
        return (
            <form className="bottom-margin-md">
                <FormGroup bsSize="large">
                    <FormControl type="text" placeholder="Search all movies" onChange={this.handleSearchTermUpdate} />
                </FormGroup>
            </form>
        );
    }

}

export default Search;
