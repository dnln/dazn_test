import React from "react";
import TheMovieDB from 'themoviedb-javascript-library';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { FormControl } from 'react-bootstrap';

import Search from './Search';

describe('Search', function () {
    let props, mountedSearch, server;

    const search = function () {
        if (!mountedSearch) {
            mountedSearch = mount(
                <Search {...props} />
            );
        }
        return mountedSearch;
    };

    beforeEach(function () {
        server = sinon.fakeServer.create();
        props = {
            theMovieDB: TheMovieDB,
            setMovies: undefined,
            setError: undefined
        };
        props.theMovieDB.common.api_key = 'fakeapikey';
        mountedSearch = undefined;
    });

    afterEach(function () {
        server.restore();
    });

    it('always renders a form', function () {
        const form = search().find('form');
        expect(form.length).toBeGreaterThan(0);
    });

    it('always calls setMovies with a list of popular movies on initialisation', function () {
        const fakeResponse = {
            results: [{
                title: 'popular film'
            }]
        };

        const expectedResponse = {
            movies: fakeResponse.results,
            isPopularMovies: true
        };

        server.respondWith('GET', 'http://api.themoviedb.org/3/movie/popular?api_key=fakeapikey', [200, { "Content-Type": "application/json" }, JSON.stringify(fakeResponse)]);

        props.setError = jest.fn();

        props.setMovies = function (movieList) {
            expect(movieList).toEqual(expectedResponse);
        };

        search();

        server.respond();

        // set error should be called to clear error
        expect(props.setError.mock.calls).toEqual([[null]]);
    });

    it('always calls setMovies with a list of popular movies when the search term is empty', function () {
        const fakeResponse = {
            results: [{
                title: 'popular film'
            }]
        };

        const expectedResponse = {
            movies: fakeResponse.results,
            isPopularMovies: true
        };

        server.respondWith('GET', 'http://api.themoviedb.org/3/movie/popular?api_key=fakeapikey', [200, { "Content-Type": "application/json" }, JSON.stringify(fakeResponse)]);

        props.setMovies = jest.fn();
        props.setError = jest.fn();

        const searchComponent = search();
        searchComponent.find(FormControl).node.props.onChange({ target: { value: '' } });

        server.respond();

        expect(props.setMovies.mock.calls).toEqual([[expectedResponse], [expectedResponse]]);
        expect(props.setError.mock.calls).toEqual([[null], [null]]);
    });

    it('always calls setMovies with a list of search results when the search term is entered', function () {
        const fakeResponse = {
            results: [{
                title: 'search query film'
            }]
        };

        const expectedResponse = {
            movies: fakeResponse.results,
            isPopularMovies: false
        };

        server.respondWith('GET', 'http://api.themoviedb.org/3/search/movie?api_key=fakeapikey&query=awesome%20movie', [200, { "Content-Type": "application/json" }, JSON.stringify(fakeResponse)]);

        props.setMovies = jest.fn();
        props.setError = jest.fn();

        const searchComponent = search();
        searchComponent.find(FormControl).node.props.onChange({ target: { value: 'awesome movie' } });

        server.respond();

        expect(props.setMovies.mock.calls).toEqual([[expectedResponse]]);
        // setError should have been called for popular movie query, as response isn't returned
        expect(props.setMovies.mock.calls.length).toEqual(1);
    });

    it('always calls setError with an error when the search query returns an error', function () {
        server.respondWith('GET', 'http://api.themoviedb.org/3/search/movie?api_key=fakeapikey&query=awesome%20movie', [500, { "Content-Type": "application/json" }, "Server error"]);

        props.setMovies = jest.fn();
        props.setError = jest.fn();

        const searchComponent = search();
        searchComponent.find(FormControl).node.props.onChange({ target: { value: 'awesome movie' } });

        server.respond();

        expect(props.setMovies.mock.calls.legth).toEqual(undefined);
        // setError should have been called for popular movie query, as response isn't returned
        expect(props.setError.mock.calls.length).toEqual(2);
        expect(props.setError.mock.calls).toEqual([[''], ['Server error']]);
    });

    it('always calls setError with an error when the popular movies query returns an error', function () {
        server.respondWith('GET', 'http://api.themoviedb.org/3/movie/popular?api_key=fakeapikey', [500, { "Content-Type": "application/json" }, "Server error"]);

        props.setMovies = jest.fn();
        props.setError = jest.fn();

        search();

        server.respond();

        expect(props.setMovies.mock.calls.legth).toEqual(undefined);
        // setError should have been called for popular movie query, as response isn't returned
        expect(props.setError.mock.calls.length).toEqual(1);
        expect(props.setError.mock.calls).toEqual([['Server error']]);
    });
});
