import React from 'react';
import { Well, Image, Row, Col } from 'react-bootstrap';

class MovieSummary extends React.Component {
    render () {
        return (
            <Well className="text-left">
                <Row>
                    <Col md={4}>
                        <Image src={this.props.movie.poster_path ? ("https://image.tmdb.org/t/p/w500/" + this.props.movie.poster_path) : ("images/no_movie_poster.jpg")} rounded responsive/>
                    </Col>
                    <Col md={8}>
                        <h4>{this.props.movie.title}</h4>
                        <p>{this.props.movie.overview}</p>
                    </Col>
                </Row>
            </Well>
        );
    }
}

export default MovieSummary;
