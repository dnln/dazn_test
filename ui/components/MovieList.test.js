import React from 'react';
import { mount } from 'enzyme';

import MovieList from './MovieList';
import MovieSummary from './MovieSummary';

describe('MovieList', function () {
    let props, mountedMovieList;

    const movieList = function () {
        if (!mountedMovieList) {
            mountedMovieList = mount(
                <MovieList {...props} />
            );
        }
        return mountedMovieList;
    };

    beforeEach(function () {
        props = {
            movieList: {
                isPopularMovies: false,
                movies: []
            }
        };
        mountedMovieList = undefined;
    });

    it('always renders a div', function () {
        const movieListComponent = movieList().find('div');
        expect(movieListComponent.length).toEqual(1);
    });

    it('always returns a message if no movies are sent to it', function () {
        const movieListComponent = movieList().find('p');
        expect(movieListComponent.text()).toEqual(expect.stringContaining('No movies found'));
    });

    it('always returns a MovieSummary component for each movie in the array', function () {
        props.movieList.movies = [
            {
                id: 1
            },
            {
                id: 2
            },
            {
                id: 3
            }
        ];

        const movieListComponent = movieList().find(MovieSummary);
        expect(movieListComponent.length).toEqual(3);
    });

    it('always sets the popular movies header if the results are popular movies', function () {
        props.movieList.isPopularMovies = true;

        const movieListComponent = movieList().find('h3');
        expect(movieListComponent.text()).toEqual('Popular movies');
    });

    it('always sets the search results header if the results are search results', function () {
        props.movieList.isPopularMovies = false;

        const movieListComponent = movieList().find('h3');
        expect(movieListComponent.text()).toEqual('Search results');
    });
});
