import React from 'react';
import { mount } from 'enzyme';
import { Grid } from 'react-bootstrap';

import App from './App';
import Header from './Header';
import Search from './Search';
import MovieList from './MovieList';
import ErrorStatus from './ErrorStatus';

describe('App', function () {
    let props, mountedApp;

    const app = function () {
        if (!mountedApp) {
            mountedApp = mount(
                <App {...props} />
            );
        }
        return mountedApp;
    };

    beforeEach(function () {
        mountedApp = undefined;
    });

    it('always renders a Grid', function () {
        const appComponent = app().find(Grid);
        expect(appComponent.length).toEqual(1);
    });

    it('always renders a Header', function () {
        const appComponent = app().find(Header);
        expect(appComponent.length).toEqual(1);
    });

    it('always renders a Search', function () {
        const appComponent = app().find(Search);
        expect(appComponent.length).toEqual(1);
    });

    it('always renders a MovieList', function () {
        const appComponent = app().find(MovieList);
        expect(appComponent.length).toEqual(1);
    });

    it('always renders an ErrorStatus', function () {
        const appComponent = app().find(ErrorStatus);
        expect(appComponent.length).toEqual(1);
    });

    it('always updates the movie state when setMovies is called', function () {
        const appComponent = app();
        expect(appComponent.state()).toEqual({ error: "", movieList: { movies: [], isPopularMovies: true } });
        appComponent.instance().setMovies({ movies: [{ id: 1 }], isPopularMovies: false });
        expect(appComponent.state()).toEqual({ error: "", movieList: { movies: [{ id: 1 }], isPopularMovies: false } });
    });
});
