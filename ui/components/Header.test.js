import React from 'react';
import { mount } from 'enzyme';

import Header from './Header';

describe('Header', function () {
    let props, mountedHeader;

    const header = function () {
        if (!mountedHeader) {
            mountedHeader = mount(
                <Header />
            );
        }
        return mountedHeader;
    };

    beforeEach(function () {
        mountedHeader = undefined;
    });

    it('always renders a h1', function () {
        const headerComponent = header().find('h1');
        expect(headerComponent.length).toEqual(1);
    });

    it('always had text in the header', function () {
        const headerComponent = header().find('h1');
        expect(headerComponent.text().length).toBeGreaterThan(0);
    });
});
