import React from 'react';
import { Alert } from 'react-bootstrap';

class ErrorStatus extends React.Component {

    render () {
        return (
            this.props.error ? (
                <Alert bsStyle="danger">
                    <strong>Error!</strong> {this.props.error}
                </Alert>
            ) : (null)
        );
    }

}

export default ErrorStatus;
