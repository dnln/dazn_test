import React from 'react';
import TheMovieDB from 'themoviedb-javascript-library';
import { Grid, Row, Col } from 'react-bootstrap';

import Header from './Header.js';
import Search from './Search.js';
import MovieList from './MovieList.js';
import ErrorStatus from './ErrorStatus.js';

TheMovieDB.common.api_key = 'c29c5861b62dcf2efaaa2ea2bbd13645';

class App extends React.Component {
    constructor () {
        super();

        this.state = {
            movieList: {
                movies: [],
                isPopularMovies: true
            },
            error: ''
        };

        this.setMovies = this.setMovies.bind(this);
        this.setError = this.setError.bind(this);
    }
    // called by Search to pass movie list to MovieList
    setMovies (movieList) {
        const newState = this.state;
        newState.movieList = movieList;
        this.setState(newState);
    }
    // called by Search to pass errors to ErrorStatus
    setError (error) {
        const newState = this.state;
        newState.error = error;
        this.setState(newState);
    }
    render () {
        return (
            <Grid>
                <Row className="show-grid">
                    <Col md={6} mdOffset={3}>
                        <div className="text-center">
                            <Header />
                            <ErrorStatus error={this.state.error}/>
                            <Search theMovieDB={TheMovieDB} setMovies={this.setMovies} setError={this.setError}/>
                            <MovieList movieList={this.state.movieList} />
                        </div>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default App;
