import React from 'react';

class Header extends React.Component {
    render () {
        return (
            <h1 className="bottom-margin-md">
                Movie searcher
            </h1>
        );
    }
}

export default Header;
