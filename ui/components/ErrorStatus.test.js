import React from 'react';
import { Alert } from 'react-bootstrap';
import { mount } from 'enzyme';

import ErrorStatus from './ErrorStatus';

describe('ErrorStatus', function () {
    let props, mountedErrorStatus;

    const errorStatus = function () {
        if (!mountedErrorStatus) {
            mountedErrorStatus = mount(
                <ErrorStatus {...props} />
            );
        }
        return mountedErrorStatus;
    };

    beforeEach(function () {
        props = {
            error: ''
        };
        mountedErrorStatus = undefined;
    });

    it('always renders an alert if error is populated', function () {
        props.error = 'this is an error message';

        const errorStatusComponent = errorStatus().find(Alert);
        expect(errorStatusComponent.length).toEqual(1);
    });

    it('always renders no alert if error isn\'t populated', function () {
        props.error = '';

        const errorStatusComponent = errorStatus().find(Alert);
        expect(errorStatusComponent.length).toEqual(0);
    });
});
