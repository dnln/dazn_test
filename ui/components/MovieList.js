import React from 'react';
import MovieSummary from './MovieSummary.js';

class MovieList extends React.Component {
    render () {
        return (
            <div>
                <h3>{this.props.movieList.isPopularMovies ? ("Popular movies") : ("Search results")}</h3>
                {this.props.movieList.movies.length > 0 ? (
                    this.props.movieList.movies.map(function (movie) {
                        return (
                            <MovieSummary key={movie.id} movie={movie}/>
                        );
                    })
                ) : (
                    <p>No movies found for your search term :(</p>
                )}

            </div>
        );
    }
}

export default MovieList;
