import React from 'react';
import { mount } from 'enzyme';
import { Well, Image } from 'react-bootstrap';

import MovieSummary from './MovieSummary';

describe('MovieSummary', function () {
    let props, mountedMovieSummary;

    const movieSummary = function () {
        if (!mountedMovieSummary) {
            mountedMovieSummary = mount(
                <MovieSummary {...props} />
            );
        }
        return mountedMovieSummary;
    };

    beforeEach(function () {
        props = {
            movie: {}
        };
        mountedMovieSummary = undefined;
    });

    it('always renders a Well', function () {
        const movieSummaryResult = movieSummary().find(Well);
        expect(movieSummaryResult.length).toEqual(1);
    });

    it('always displays the poster image if poster_path is populated', function () {
        props.movie.poster_path = "a.jpg";

        const movieSummaryResult = movieSummary().find(Image).node.props.src;
        expect(movieSummaryResult).toEqual("https://image.tmdb.org/t/p/w500/a.jpg");
    });

    it('always displays a "poster not found" image if poster_path is not populated', function () {
        const movieSummaryResult = movieSummary().find(Image).node.props.src;
        expect(movieSummaryResult).toEqual("images/no_movie_poster.jpg");
    });

    it('always displays the movie title if the title is populated', function () {
        props.movie.title = "awesome movie";

        const movieSummaryResult = movieSummary().find('h4');
        expect(movieSummaryResult.text()).toEqual(props.movie.title);
    });

    it('always displays the movie overview if the title is overview', function () {
        props.movie.overview = "This is an awesome movie";

        const movieSummaryResult = movieSummary().find('p');
        expect(movieSummaryResult.text()).toEqual(props.movie.overview);
    });
});
